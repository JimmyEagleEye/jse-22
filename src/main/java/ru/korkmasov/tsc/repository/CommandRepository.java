package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.ICommandRepository;
import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.util.ValidationUtil;
import static ru.korkmasov.tsc.util.ValidationUtil.isEmpty;


import java.util.*;
import java.util.stream.Collectors;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public Collection<String> getCommandNames() {
        return commands.values().stream()
                .map(AbstractCommand::name)
                .filter(name -> !isEmpty(name))
                .collect(Collectors.toList());
    }

    @Override
    public Collection<String> getCommandArgs() {
        return commands.values().stream()
                .map(AbstractCommand::arg)
                .filter(arg -> !isEmpty(arg))
                .collect(Collectors.toList());
    }

    @Override
    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String name) {
        return arguments.get(name);
    }

    @Override
    public void add(AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

}
