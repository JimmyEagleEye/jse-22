package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.IProjectRepository;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.model.Project;

public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    public String getIdByName(final String userId, final String name) {
        for (final Project project : list) {
            if (name.equals(project.getName())) return project.getId();
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public boolean existsByName(final String userId, final String name) {
        for (final Project project : list) {
            if (name.equals(project.getName()) && userId.equals(project.getName())) return true;
        }
        return false;
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        int i = 0;
        for (final Project project : list) {
            if (userId.equals(project.getUserId())) i++;
            if (index.equals(i)) return project;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        for (final Project project : list) {
            if (name.equals(project.getName()) && userId.equals(project.getUserId())) return project;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        remove(findOneByIndex(userId, index));
        return null;
    }

    @Override
    public Project removeOneByName(final String userId, final String name) {
        remove(findOneByName(userId, name));
        return null;
    }

}
