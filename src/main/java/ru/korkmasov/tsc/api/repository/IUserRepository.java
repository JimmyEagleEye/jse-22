package ru.korkmasov.tsc.api.repository;

import ru.korkmasov.tsc.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    User removeUser(User user);

    User removeUserById(String id);

    User removeUserByLogin(String login);

    boolean existsByLogin(String login);

    boolean existsByEmail(String email);

}
