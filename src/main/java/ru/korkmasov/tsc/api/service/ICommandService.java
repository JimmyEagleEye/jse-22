package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<String> getListArgumentName();

    Collection<String> getListCommandName();

    void add(AbstractCommand command);

}
