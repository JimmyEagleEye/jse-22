package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findALLTaskByProjectId(String userId, String projectId);

    Task assignTaskByProjectId(final String userId, final String taskId, final String projectId);


    Task unassignTaskByProjectId(String userId, String taskId);

    List<Task> removeTasksByProjectId(String userId, String projectId);

    Project removeProjectById(String userId, String projectId);

    void removeProjectByName(String userId, String projectName);
}
