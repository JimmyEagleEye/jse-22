package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.repository.IProjectRepository;
import ru.korkmasov.tsc.api.service.IAuthService;
import ru.korkmasov.tsc.api.service.IProjectService;
import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.util.ValidationUtil;

import java.util.Date;

public class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository, IAuthService authService) {
        super(projectRepository, authService);
        this.projectRepository = projectRepository;
    }

    @Override
    public Project findOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(userId, id);
    }

    @Override
    public Project add(final String userId, final String name, final String description) {
        if (ValidationUtil.isEmpty(name)) throw new EmptyNameException();
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeById(userId, id);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }


    @Override
    public Project removeOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(userId, name);
    }

    @Override
    public Project removeProjectByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > projectRepository.size(userId)) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Override
    public Project updateProjectByIndex(final String userId, final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectById(final String userId, final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findById(userId, id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByName(final String userId, final String name, final String nameNew, final String description) {
        if (ValidationUtil.isEmpty(name) || ValidationUtil.isEmpty(nameNew)) throw new EmptyNameException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(nameNew);
        project.setDescription(description);
        return project;
    }


    @Override
    public Project startProjectById(String userId, String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project startProjectByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = projectRepository.findOneByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;

    }

    @Override
    public Project startProjectByName(String userId, String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findOneByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project finishProjectById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

    @Override
    public Project finishProjectByIndex(final String userId, Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = projectRepository.findOneByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

    @Override
    public Project finishProjectByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findOneByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId, final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(final String userId, final String name, final Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(userId, name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, final Integer index, final Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public boolean existsByName(String userId, String name) {
        if (ValidationUtil.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.existsByName(userId, name);
    }

}

